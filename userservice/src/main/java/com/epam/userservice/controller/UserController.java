package com.epam.userservice.controller;


import com.epam.userservice.model.User;
import com.epam.userservice.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;
import javax.inject.Inject;
import java.util.List;

@Api(value = "User Service", description = "Provides access to add,view,update and delete user")
@RestController
@RequestMapping("/users")
public class UserController {

    @Inject
    private UserService userService;

    @ApiOperation(value = "Views a list of all Users",response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code=200,message="Successfully retrieved user list")
    })
    @GetMapping
    public List<User> getUsers()
    {
        return userService.findAllUsers();
    }

    @ApiOperation(value = "Views an User based on the given id value",response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code=200,message="Successfully retrieved user based on the given id"),
            @ApiResponse(code=404,message = "Sorry,User not found with the given Id")
    })
    @GetMapping("/{id}")
    public User getUserById(@PathVariable(value = "id") long id)
    {
        return userService.findUserById(id);
    }

    @ApiOperation(value = "Adds an User and returns the added user",response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code=200,message="Successfully added user")
    })
    @PostMapping
    public User addUser(@RequestBody User user)
    {
        return userService.addNewUser(user);
    }

    @ApiOperation(value = "Removes an User and returns the success message",response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code=200,message="Successfully deleted user"),
            @ApiResponse(code=404,message="Could not delete user,No User found with the given Id")
    })
    @DeleteMapping("/{id}")
    public String deleteUser(@PathVariable(value = "id") long id)
    {
        return userService.deleteUserById(id);
    }

    @ApiOperation(value = "Updates an User and returns the updated user",response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code=200,message="Successfully updated user"),
            @ApiResponse(code=404,message="Could not update details,No User found with the given Id")
    })
    @PutMapping("/{id}")
    public User updateUser(@PathVariable(value = "id") long id,@RequestBody User user) {
        return userService.updateUser(id,user);
    }

}
