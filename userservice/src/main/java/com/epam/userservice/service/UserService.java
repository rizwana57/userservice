package com.epam.userservice.service;

import com.epam.userservice.dao.UserRepository;
import com.epam.userservice.exception.UserNotFoundException;
import com.epam.userservice.model.User;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

  @Inject
  private UserRepository userRepository;

    public List<User> findAllUsers()
    {

        List<User> users = userRepository.findAll();
        if(users.isEmpty())
            return Collections.emptyList();
        return users;
    }

    public User findUserById(long id){
        Optional<User> user = userRepository.findById(id);
        user.orElseThrow(()->new UserNotFoundException("Sorry,User not found with the given Id") );
        return user.get();
    }

    public User addNewUser(User user){
            return userRepository.save(user);
    }

    public String deleteUserById(long id){
        Optional<User> user = userRepository.findById(id);
        user.orElseThrow(()->new UserNotFoundException("Could not delete user,No User found with the given Id"));
            userRepository.deleteById(id);
            return "User deleted successfully";
    }

    public User updateUser(long id,User user) {
        Optional<User> existingUser = userRepository.findById(id);
        existingUser.orElseThrow(()->new UserNotFoundException("Could not Update details,No User found with the given Id"));
        user.setId(id);
        return userRepository.save(user);
    }

}
